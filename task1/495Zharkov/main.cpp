#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"

#include <vector>
#include <iostream>

class SampleApplication : public Application {
public:
	MeshPtr _moebiusStrip;
	ShaderProgramPtr _shader;

	unsigned detalization = 5; //����� �������� ��� ���-�� �� ����� ���� �������� ����� ������ � ������ �����������
	int mebius_r = 1.0f;

	int polygonMode = GL_FILL;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//������� ��� � ������ ̸�����
		_moebiusStrip = makeMoebiusStrip(mebius_r, detalization);
		//_moebiusStrip = makeSphere(mebius_r, detalization);
		_moebiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		_shader = std::make_shared<ShaderProgram>("495ZharkovData/shaderNormal.vert", "495ZharkovData/shader.frag");
	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);
		bool detalizationChanged = false;

		if (key == GLFW_KEY_EQUAL) {
			if (detalization < 100) {
				detalization += 5;
				detalizationChanged = true;
			}
		}

		if (key == GLFW_KEY_MINUS) {
			if (detalization > 10) {
				detalization -= 5;
				detalizationChanged = true;
			}
		}

		if (detalizationChanged) {
			_moebiusStrip = makeMoebiusStrip(mebius_r, detalization);
			//_moebiusStrip = makeSphere(mebius_r, detalization);
			_moebiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
			draw();
		}
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

		//������������� ������.
		_shader->use();

		//������������� ����� �������-����������
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//������ ���
		_shader->setMat4Uniform("modelMatrix", _moebiusStrip->modelMatrix());

		_moebiusStrip->draw();
	}
};

int main(int argc, char** argv)
{
	SampleApplication app;
	app.start();

	return 0;
}